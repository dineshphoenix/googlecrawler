
#####################

#Install Python3 
#Install the following Packages used
#Usage : python3 GoogleCrawler.py > filename 
#'filename' denotes the file to which the apk list will be crawled

#####################


#!/usr/bin/python3

from bs4 import BeautifulSoup                   #works with html pages make it human readable
from urllib.request import urlopen              #opens a given url
import urllib.parse                             #splits the url into ints components
import codecs                                   #for encoding and decoding page
import json                                     #for encoding and decoding stuff
import pickle                                   #for converting python object to byte stream and vice versa
from datetime import datetime


def loadState():
    try:
        state_file = open( "state_dump", "rb" )
        apps_discovered = pickle.load( state_file )
        apps_pending = pickle.load( state_file )
        state_file.close()
        #print( "Pending = ", len( apps_pending ), " Discovered = ", len( apps_discovered ) )
        return apps_discovered, apps_pending
    except IOError:
        #print( "A fresh start ..." )
        return [], []

character_encoding = 'utf-8'
apps_discovered, apps_pending = loadState()
count_offset = len( apps_discovered )
 
start_time = datetime.now()

def getPageAsSoup( url, post_values ):
    if post_values:
        data = urllib.parse.urlencode( post_values )
        data = data.encode( character_encoding )
        req = urllib.request.Request( url, data )
    else:
        req = url
    try:
        response = urllib.request.urlopen( req )
    except urllib.error.HTTPError as e:
        print( "HTTPError with: ", url, "\t", e )
        return None
    the_page = response.read()
    soup = BeautifulSoup( the_page ) 
    return soup

def getAppLink( url, start, num, app_type ):
    values = {'start' : start,
              'num': num,
              'numChildren':'0',
              'ipf': '1',
              'xhr': '1'}
    soup = getPageAsSoup( url, values )
        
    
    for div in soup.findAll( 'div', {'class' : 'details'} ):
        title = div.find( 'a', {'class':'title'} )
        #print (title.get( 'href' ))
         
        apks.append(title.get('href'))
   
     
def getApps( url ):

    start_idx = 0
    size = 100
    
    while(True):
        
        getAppLink( url, start_idx, size, app_type )
        
        if(len(apks)!=len(set(apks))):
        
        #if(len(apks)==200):

            for i in set(apks):
                print(i)
            del apks[:]
            break

        else:
            start_idx += size
            
 
categories = ['BOOKS_AND_REFERENCE', 'BUSINESS', 'COMICS', 'COMMUNICATION', 'EDUCATION', 'ENTERTAINMENT', 'FINANCE', 'HEALTH_AND_FITNESS', 'LIBRARIES_AND_DEMO', 'LIFESTYLE', 'APP_WALLPAPER', 'MEDIA_AND_VIDEO', 'MEDICAL', 'MUSIC_AND_AUDIO', 'NEWS_AND_MAGAZINES', 'PERSONALIZATION', 'PHOTOGRAPHY', 'PRODUCTIVITY', 'SHOPPING', 'SOCIAL', 'SPORTS', 'TOOLS', 'TRANSPORTATION', 'TRAVEL_AND_LOCAL', 'WEATHER', 'APP_WIDGETS', 'GAME_ACTION' , 'GAME_ADVENTURE' , 'GAME_ARCADE' , 'GAME_BOARD' , 'GAME_CARD' , 'GAME_CASINO' , 'GAME_CASUAL' , 'GAME_EDUCATIONAL' , 'GAME_FAMILY' , 'GAME_MUSIC' , 'GAME_PUZZLE' , 'GAME_RACING' , 'GAME_ROLE_PLAYING' , 'GAME_SIMULATION' , 'GAME_SPORTS' , 'GAME_STRATEGY' , 'GAME_TRIVIA' , 'GAME_WORD']
app_types = ['free', 'paid']
#app_types = ['free']
apks = list()
 
for category, app_type in [( x, y ) for x in categories for y in app_types]:
    print( "Type = ", app_type, " Category = ", category )
    url = 'https://play.google.com/store/apps/category/' + category + '/collection/topselling_' + app_type
    print(url)
    getApps( url )
