This repository contains the code to crawl the list of apps present in each category from the Google Play Store.

Code: GoogleCrawler.py

Before Usage:

Install python3
Install the packages used in GoogleCrawler.py

Usage: python3 GoogleCrawler.py > filename

filename --> this denotes the file to which the list of apps will be crawled